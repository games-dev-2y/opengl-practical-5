/// <summary>
/// @mainpage OpenGL Practical 5
/// @Author Rafael Girao
/// @Version 1.0
///
/// Time Spent:
///		09th/11/2016 22:00 120min (2hr)
///		10th/11/2016 13:00 240min (4hr)
///
/// Total Time Taken:
///		6hr 00min
/// </summary>


#include "Game.h"


int main(void)
{
	sf::ContextSettings settings;
	settings.depthBits = 24u;
	settings.stencilBits = 8u;
	settings.antialiasingLevel = 32u;
	settings.majorVersion = 3u;
	settings.minorVersion = 0u;

	Game& game = Game(settings);
	game.run();
}
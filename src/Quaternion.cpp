#include "Quaternion.h"


/// <summary>
/// Default constructor,
/// creates a zero quaternion
/// </summary>
Quaternion::Quaternion() :
	m_w(0.0),
	m_x(0.0),
	m_y(0.0),
	m_z(0.0)
{
}

/// <summary>
/// Overloaded constructor,
/// creates a quaternion based on passed arguments
/// </summary>
/// <param name="w"> defines w component </param>
/// <param name="x"> defines x component </param>
/// <param name="y"> defines y component </param>
/// <param name="z"> defines z component </param>
Quaternion::Quaternion(double w, double x, double y, double z) :
	m_w(w),
	m_x(x),
	m_y(y),
	m_z(z)
{
}

/// <summary>
/// Overloaded constructor,
/// creates a quaternion in a angle/axis format
/// </summary>
/// <param name="w"> angle </param>
/// <param name="v"> axis </param>
Quaternion::Quaternion(double w, Vector3 v) :
	m_w(w),
	m_x(v.getX()),
	m_y(v.getY()),
	m_z(v.getZ())
{
}

/// <summary>
/// Gets the w component
/// </summary>
double Quaternion::getW() const
{
	return m_w;
}

/// <summary>
/// Gets the x component
/// </summary>
double Quaternion::getX() const
{
	return m_x;
}

/// <summary>
/// Gets the y component
/// </summary>
double Quaternion::getY() const
{
	return m_y;
}

/// <summary>
/// Gets the z component
/// </summary>
double Quaternion::getZ() const
{
	return m_z;
}

/// <summary>
/// Calculates the modulus of the quaternion,
/// using the following formula:
/// modulus = sqrt(w*w + x*x + y*y + z*z)
/// </summary>
/// <returns> returns a double as the modulus </returns>
double Quaternion::modulus() const
{
	return sqrt(m_w * m_w + m_x * m_x + m_y * m_y + m_z * m_z);
}

/// <summary>
/// Calculates the modulus squared,
/// using the following formula:
/// modulusSquared = (w*w + x*x + y*y + z*z)
/// </summary>
/// <returns> returns a double as the modulus squared </returns>
double Quaternion::modulusSquared() const
{
	return (m_w * m_w + m_x * m_x + m_y * m_y + m_z * m_z);
}

/// <summary>
/// Gets the normalized quaternion
/// </summary>
/// <returns> returns a quaternion as the normalized quaternion </returns>
Quaternion Quaternion::normalise() const
{
	double magnitude = modulusSquared();
	if (magnitude > 0.001)
	{
		magnitude = sqrt(magnitude);
		return Quaternion(m_w / magnitude, m_x / magnitude, m_y / magnitude, m_z / magnitude);
	}
	else
	{
		return Quaternion(1.0, 0.0, 0.0, 0.0);
	}
}

/// <summary>
/// Negate x/y/z components
/// </summary>
/// <returns> returns a quaternion as the conjugate </returns>
Quaternion Quaternion::conjugate() const
{
	return Quaternion(m_w, -m_x, -m_y, -m_z);
}

/// <summary>
/// mulitply the conjugate by (1/length^2)
/// </summary>
/// <returns> returns the inverse of the quaternion </returns>
Quaternion Quaternion::inverse() const
{
	return (conjugate() * (1 / modulusSquared()));
}

/// <summary>
/// Multiplies the passed Quaternion
/// </summary>
/// <param name="q"> passed quaternion </param>
/// <returns> returns the product of 2 quaternions </returns>
Quaternion Quaternion::multiply(const Quaternion & q) const
{
	return Quaternion(
		q.m_w *  m_w - q.m_x *  m_x - q.m_y *  m_y - q.m_z *  m_z,
		q.m_w *  m_x + q.m_x *  m_w + q.m_y *  m_z - q.m_z *  m_y,
		q.m_w *  m_y + q.m_y *  m_w + q.m_z *  m_x - q.m_x *  m_z,
		q.m_w *  m_z + q.m_z *  m_w + q.m_x *  m_y - q.m_y *  m_x);
}

/// <summary>
/// Creates a transformed vector,
/// that represents a vector that has been rotated about this quaternion
/// </summary>
/// <param name="v"> vector to be rotated </param>
/// <param name="angle"> angle of rotation in degrees </param>
/// <returns> returns the transformed vector </returns>
Vector3 Quaternion::rotate(const Vector3 & v, const double & angle) const
{
	Quaternion q(normalise());
	double angleRad = angle * (acos(-1) / 180);
	double angSin = sin(angleRad / 2);

	q = Quaternion(cos(angleRad / 2), angSin * q.m_x, angSin * q.m_y, angSin * q.m_z);

	Quaternion qInverse(q.inverse());
	Quaternion qV(0.0, v.getX(), v.getY(), v.getZ());
	q = q * qV * qInverse;

	return Vector3(q.m_x, q.m_y, q.m_z);
}

/// <summary>
/// Creates a quaternion that will represent a rotation about a axis
/// </summary>
/// <param name="angle"> angle in degrees </param>
/// <param name="axis"> axis to rotate about </param>
/// <returns> returns a quaternion in a angle/axis format </returns>
Quaternion Quaternion::angleAxis(const double & angle, const Vector3 & axis)
{
	double angleRad = acos(-1) / 180;
	double sinTheta = sin(angleRad / 2);
	return Quaternion(cos(angleRad / 2), sinTheta * axis.getX(), sinTheta * axis.getY(), sinTheta * axis.getZ());
}

/// <summary>
/// Gets a quaternion formatted into a string
/// </summary>
/// <returns> returns a string containing the quaternion </returns>
std::string Quaternion::toString() const
{
	return std::string(
		"(" + std::to_string(m_w) + "," + std::to_string(m_x) + "i," + std::to_string(m_y) + "j," + std::to_string(m_z) + "k)"
	);
}

/// <summary>
/// Adds 2 quaternions together
/// </summary>
/// <param name="q"> 2nd quaternion </param>
/// <returns> returns a quaternion as the sum of 2 quaternions </returns>
Quaternion operator+(const Quaternion & lhs, const Quaternion & rhs)
{
	return Quaternion(lhs.m_w + rhs.m_w, lhs.m_x + rhs.m_x, lhs.m_y + rhs.m_y, lhs.m_z + rhs.m_z);
}

/// <summary>
/// Negates the quaternion
/// </summary>
/// <returns> returns a negated quaternion </returns>
Quaternion operator-(const Quaternion & lhs)
{
	return Quaternion(-lhs.m_w, -lhs.m_x, -lhs.m_y, -lhs.m_z);
}

/// <summary>
/// Subtracts 2 quaternions
/// </summary>
/// <param name="q"> 2nd quaternion </param>
/// <returns> returns a quaternion as the difference between 2 quaternions </returns>
Quaternion operator-(const Quaternion & lhs, const Quaternion & rhs)
{
	return Quaternion(lhs.m_w - rhs.m_w, lhs.m_x - rhs.m_x, lhs.m_y - rhs.m_y, lhs.m_z - rhs.m_z);
}

/// <summary>
/// Multiplies 2 quaternions together
/// </summary>
/// <param name="q"> 2nd quaternion </param>
/// <returns> returns the product between 2 quaternions </returns>
Quaternion operator*(const Quaternion & lhs, const Quaternion & rhs)
{
	return Quaternion(rhs.multiply(lhs));
}

/// <summary>
/// Multiplies the quaternion by a scalar
/// </summary>
/// <param name="scalar"> double to multiply </param>
/// <returns> returns the product of a quaternion by a scalar </returns>
Quaternion operator*(const Quaternion & lhs, const double & rhs)
{
	return Quaternion(lhs.m_w * rhs, lhs.m_x * rhs, lhs.m_y * rhs, lhs.m_z * rhs);
}

/// <summary>
/// Multiplies the quaternion by a scalar
/// </summary>
/// <param name="scalar"> float to multiply </param>
/// <returns> returns the product of a quaternion by a scalar </returns>
Quaternion operator*(const Quaternion & lhs, const float & rhs)
{
	return Quaternion(lhs.m_w * rhs, lhs.m_x * rhs, lhs.m_y * rhs, lhs.m_z * rhs);
}

/// <summary>
/// Multiplies the quaternion by a scalar
/// </summary>
/// <param name="scalar"> int to multiply </param>
/// <returns> returns the product of a quaternion by a scalar </returns>
Quaternion operator*(const Quaternion & lhs, const int & rhs)
{
	return Quaternion(lhs.m_w * rhs, lhs.m_x * rhs, lhs.m_y * rhs, lhs.m_z * rhs);
}

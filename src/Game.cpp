#include "Game.h"

bool flip = false;
int current = 1;

Game::Game(sf::ContextSettings settings) :
	m_window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Default, settings),
	m_primatives(2),
	m_rotationAngle(0.0f),
	m_isRunning(false),
	m_zoomOut(false),
	m_quatRotation(0.0, 0.0, 0.0, 1.0),
	m_prim1(),
	m_prim2()
{
	m_index = glGenLists(m_primatives);
}

Game::~Game() {}

void Game::run()
{
	initialize();

	sf::Event event;

	while (m_isRunning) {

		std::cout << "Game running..." << std::endl;

		while (m_window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				m_isRunning = false;
				break;
			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
				case sf::Keyboard::Space:
					m_zoomOut = !m_zoomOut;
					break;
				case sf::Keyboard::Up:
					m_transformation = Matrix3::scale(1.1, 1.1);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_transformation * pt;
					}
					updatePrim1();
					updatePrim2();
					break;
				case sf::Keyboard::Down:
					m_transformation = Matrix3::scale(0.9, 0.9);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_transformation * pt;
					}
					updatePrim1();
					updatePrim2();
					break;
				case sf::Keyboard::Right:
					m_transformation = Matrix3::rotation(Matrix3::Axis::Z, 5.0);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_quatRotation.rotate(pt, 5.0);
					}
					updatePrim1();
					updatePrim2();
					break;
				case sf::Keyboard::Left:
					m_transformation = Matrix3::rotation(Matrix3::Axis::Z, -5.0);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_quatRotation.rotate(pt, -5.0);
					}
					updatePrim1();
					updatePrim2();
					break;
				case sf::Keyboard::D:
					m_transformation = Matrix3::translate(-0.05, 0.0);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_transformation * pt;
					}
					updatePrim1();
					updatePrim2();
					break;
				case sf::Keyboard::A:
					m_transformation = Matrix3::translate(0.05, 0.0);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_transformation * pt;
					}
					updatePrim1();
					updatePrim2();
					break;
				case sf::Keyboard::W:
					m_transformation = Matrix3::translate(0.0, -0.05);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_transformation * pt;
					}
					updatePrim1();
					updatePrim2();
					break;
				case sf::Keyboard::S:
					m_transformation = Matrix3::translate(0.0, 0.05);
					for (auto & pt : m_prim1)
					{
						pt = m_transformation * pt;
					}
					for (auto & pt : m_prim2)
					{
						pt = m_transformation * pt;
					}
					updatePrim1();
					updatePrim2();
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
		update();
		draw();
	}

}

void Game::initialize()
{
	m_isRunning = true;

	m_prim1[0] = Vector3(0.0, 2.0, -5.0);
	m_prim1[1] = Vector3(-2.0, -2.0, -5.0);
	m_prim1[2] = Vector3(2.0, -2.0, -5.0);

	m_prim2[0] = Vector3(0.2, 0.0, -2.0);
	m_prim2[1] = Vector3(-2.0, -2.0, -2.0);
	m_prim2[2] = Vector3(2.0, -2.0, -2.0);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, m_window.getSize().x / m_window.getSize().y, 1.0, 500.0);
	glMatrixMode(GL_MODELVIEW);

	updatePrim1();
	updatePrim2();
}

void Game::update()
{
	m_elapsed = m_clock.getElapsedTime();

	if (m_elapsed.asSeconds() >= 1.0f)
	{
		m_clock.restart();

		if (!flip)
		{
			flip = true;
			current++;
			if (current > m_primatives)
			{
				current = 1;
			}
		}
		else
			flip = false;
	}

	if (flip)
	{
		m_rotationAngle += 0.05f;

		if (m_rotationAngle > 360.0f)
		{
			m_rotationAngle -= 360.0f;
		}
	}

	std::cout << "Update up" << std::endl;
}

void Game::draw()
{
	std::cout << "Draw up" << std::endl;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (!m_zoomOut)
	{
		glLoadIdentity(); // Resets camera transformations (required for proper rotations)
		glTranslatef(0.0f, 0.0f, -4.0f); // Transforms camera by moving it a set amount
	}
	else
	{
		glTranslatef(0.0f, 0.0f, -0.01f); // Transforms camera by moving it a set amount
	}

	std::cout << "Drawing Primative " << current << std::endl;
	glCallList(current);
	
	m_window.display();

}

void Game::unload()
{
	std::cout << "Cleaning up" << std::endl;
}

void Game::updatePrim1()
{
	glNewList(m_index, GL_COMPILE);
	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.0f, 0.0f, 1.0f);
		for (auto & point : m_prim1)
		{
			glVertex3d(point.getX(), point.getY(), point.getZ());
		}
	}
	glEnd();
	glEndList();
}

void Game::updatePrim2()
{
	glNewList(m_index + 1, GL_COMPILE);
	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.0f, 1.0f, 0.0f);
		for (auto & point : m_prim2)
		{
			glVertex3d(point.getX(), point.getY(), point.getZ());
		}
	}
	glEnd();
	glEndList();
}


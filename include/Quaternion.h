#pragma once

#include "Vector3.h"

class Quaternion
{
	friend Quaternion operator+(const Quaternion&, const Quaternion&);
	friend Quaternion operator-(const Quaternion&);
	friend Quaternion operator-(const Quaternion&, const Quaternion&);
	friend Quaternion operator*(const Quaternion&, const Quaternion&);
	friend Quaternion operator*(const Quaternion&, const double&);
	friend Quaternion operator*(const Quaternion&, const float&);
	friend Quaternion operator*(const Quaternion&, const int&);

public:
	Quaternion();
	Quaternion(double, double, double, double);
	Quaternion(double, Vector3);

	double getW() const;
	double getX() const;
	double getY() const;
	double getZ() const;

	double modulus() const;
	double modulusSquared() const;
	Quaternion normalise() const;
	Quaternion conjugate() const;
	Quaternion inverse() const;
	Quaternion multiply(const Quaternion&) const;
	Vector3 rotate(const Vector3&, const double&) const;

	static Quaternion angleAxis(const double&, const Vector3&);

	std::string toString() const;

private:
	double m_w;
	double m_x;
	double m_y;
	double m_z;
};


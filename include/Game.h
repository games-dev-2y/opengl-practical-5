#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
/// <summary>
/// This will hookup OpenGL
/// to our game
/// </summary>
#include <gl/GL.h>
#include <gl/GLU.h>

#include "Vector3.h"
#include "Matrix3.h"
#include "Quaternion.h"

class Game
{
public:
	Game(sf::ContextSettings settings = sf::ContextSettings());
	~Game();
	void run();
private:
	void initialize();
	void update();
	void draw();
	void unload();

	void updatePrim1();
	void updatePrim2();

	sf::Window m_window;
	bool m_isRunning;
	bool m_zoomOut;
	const int m_primatives;
	GLuint m_index;
	sf::Clock m_clock;
	sf::Time m_elapsed;
	
	Quaternion m_quatRotation;

	Matrix3 m_transformation;

	Vector3 m_prim1[3];
	Vector3 m_prim2[3];

	float m_rotationAngle;
};